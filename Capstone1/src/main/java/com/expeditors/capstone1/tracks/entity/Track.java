package com.expeditors.capstone1.tracks.entity;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.List;
import com.expeditors.capstone1.artists.entity.Artist;

@Entity
@Table(name = "tracks")
public class Track {

    public enum MediaType {
        OGG,
        MP3,
        FLAC,
        WAV
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer trackID;

    private String title;
    private String artist;
    private LocalDate issuedDate;
    private MediaType mediaType;
    private Integer duration;

    // Empty constructor
    public Track() {}

    // Constructor
    public Track(Integer trackID, String title, String artist, LocalDate issuedDate, MediaType mediaType, Integer duration) {
        this.trackID = trackID;
        this.title = title;
        this.artist = artist;
        this.issuedDate = issuedDate;
        this.mediaType = mediaType;
        this.duration = duration;
    }

    public Integer getTrackID() {
        return trackID;
    }

    public void setTrackID(Integer trackID) {
        this.trackID = trackID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public LocalDate getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(LocalDate issuedDate) {
        this.issuedDate = issuedDate;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Track [id=" + trackID +", title=" + title + ", artist=" + artist + ", issuedDate=" + issuedDate + ", mediatype=" + mediaType + ", duration=" + duration + "]";
    }
}
