package com.expeditors.capstone1.tracks.controller;

import java.util.List;
import com.expeditors.capstone1.tracks.repository.TrackRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.repository.query.Param;
import org.springframework.ui.Model;
import com.expeditors.capstone1.tracks.repository.TrackRepository;
import com.expeditors.capstone1.tracks.entity.Track;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.expeditors.capstone1.tracks.service.TrackService;

@RestController
@RequestMapping("/api/v1")
public class TrackController {
    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private TrackService trackService;

    // Method to get all tracks
    @GetMapping("/tracks")
    public ResponseEntity<List<Track>> getAllTracks() {
        List<Track> tracks = trackService.getAllTracks();
        return new ResponseEntity<>(tracks, HttpStatus.OK);
    }

    /* SAVE FOR NOW - DELETE LATER
    public List <Track> getAllTracks() {
        return trackRepository.findAll();
    }
    */

    // Method to save a new track
    @PostMapping("/tracks/create")
    public ResponseEntity<Track> createTrack(@RequestBody Track track) {
        Track newTrack = trackService.createTrack(track);
        return new ResponseEntity<>(newTrack, HttpStatus.CREATED);
    }

    /* SAVE FOR NOW - DELETE LATER
    public Track createTrack(@Valid @RequestBody Track track) {
        return trackRepository.save(track);
    }
    */

    // Method to get track by id
    @GetMapping("/tracks/{id}")
    public ResponseEntity<Track> getTrackById(@PathVariable("id") Integer trackId) {
        Track track = trackService.getTrackById(trackId);
        return new ResponseEntity<>(track, HttpStatus.OK);
    }

    // Method to edit a track
    @PutMapping("/tracks/edit/{id}")
    public ResponseEntity<Track> updateTrack(@PathVariable("id") Integer id, @RequestBody Track track) {
        track.setTrackID(id);
        Track updateTrack = trackService.updateTrack(track);
        return new ResponseEntity<>(updateTrack, HttpStatus.OK);

    }

    // Method to delete a track
    @DeleteMapping("/tracks/delete/{id}")
    public ResponseEntity<String> deleteTrack(@PathVariable("id") Integer trackId) {
        trackRepository.deleteById(trackId);
        return new ResponseEntity<>("Track successfully deleted!", HttpStatus.OK);
    }

}
