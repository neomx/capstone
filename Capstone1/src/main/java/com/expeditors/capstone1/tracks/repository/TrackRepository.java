package com.expeditors.capstone1.tracks.repository;

import com.expeditors.capstone1.tracks.entity.Track;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
@Transactional
public interface TrackRepository extends JpaRepository<Track, Integer> {
    List<Track> findByTitleContainingIgnoreCase(String keyword);

}
