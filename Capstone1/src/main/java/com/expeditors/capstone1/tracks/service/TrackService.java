package com.expeditors.capstone1.tracks.service;

import java.util.List;
import com.expeditors.capstone1.tracks.entity.Track;

public interface TrackService {
    Track createTrack(Track track);

    Track getTrackById(Integer trackId);

    List<Track> getAllTracks();

    Track updateTrack(Track track);

    void deleteTrack(Integer trackId);

}
