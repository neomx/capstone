package com.expeditors.capstone1.tracks.service.impl;

import lombok.AllArgsConstructor;
import java.util.List;
import com.expeditors.capstone1.tracks.entity.Track;
import com.expeditors.capstone1.tracks.repository.TrackRepository;
import com.expeditors.capstone1.tracks.service.TrackService;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TrackServiceImpl implements TrackService {
    private TrackRepository trackRepository;

    @Override
    public Track createTrack(Track track) {
        return trackRepository.save(track);
    }

    @Override
    public Track getTrackById(Integer trackId) {
        Optional<Track> optionalTrack = trackRepository.findById(trackId);
        return optionalTrack.get();
    }

    @Override
    public List<Track> getAllTracks() {
        return trackRepository.findAll();
    }

    @Override
    public Track updateTrack(Track track) {
        Track existingTrack = trackRepository.findById(track.getTrackID()).get();
        existingTrack.setTitle(track.getTitle());
        Track updateTrack = trackRepository.save(existingTrack);
        return updateTrack;
    }

    @Override
    public void deleteTrack(Integer trackID) {
        trackRepository.deleteById(trackID);
    }


}
