package com.expeditors.capstone1.artists.service;

import java.util.List;
import com.expeditors.capstone1.artists.entity.Artist;
import com.expeditors.capstone1.tracks.entity.Track;

public interface ArtistService {
    Artist createArtist(Artist artist);

    List<Artist> getAllArtists();

    Artist getArtistById(Integer artistId);

    Artist updateArtist(Artist artist);

    void deleteArtist(Integer artistId);

}
