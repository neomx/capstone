package com.expeditors.capstone1.artists.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import com.expeditors.capstone1.artists.entity.Artist;

@Repository
@Transactional
public interface ArtistRepository extends JpaRepository<Artist, Integer> {

}
