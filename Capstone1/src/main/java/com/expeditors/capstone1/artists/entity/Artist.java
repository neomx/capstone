package com.expeditors.capstone1.artists.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "artists")
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer artistId;
    private String firstName;
    private String lastName;
    private String country;

    // Empty constructor
    public Artist() {}

    // Constructor
    public Artist(Integer artistId, String firstName, String lastName, String country) {
        this.artistId = artistId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Artist [id=" + artistId +", first=" + firstName + ", last=" + lastName + ", country=" + country + "]";
    }


}
