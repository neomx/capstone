package com.expeditors.capstone1.artists.controller;

import java.util.List;

import com.expeditors.capstone1.artists.repository.ArtistRepository;
import com.expeditors.capstone1.tracks.entity.Track;
import com.expeditors.capstone1.tracks.repository.TrackRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import com.expeditors.capstone1.artists.entity.Artist;
import com.expeditors.capstone1.artists.service.ArtistService;

@RestController
@RequestMapping("/api/v1")
public class ArtistController {
    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ArtistService artistService;

    // Method to create a new artist
    @PostMapping("/artists/create")
    public ResponseEntity<Artist> createArtist(@RequestBody Artist artist) {
        Artist newArtist = artistService.createArtist(artist);
        return new ResponseEntity<>(newArtist, HttpStatus.CREATED);
    }

    // Method to get all artists
    @GetMapping("/artists")
    public ResponseEntity<List<Artist>> getAllArtists() {
        List<Artist> artists = artistService.getAllArtists();
        return new ResponseEntity<>(artists, HttpStatus.OK);
    }

    // Methods to get an artist by id
    @GetMapping("/artists/{id}")
    public ResponseEntity<Artist> getArtistById(@PathVariable("id") Integer artistId) {
        Artist artist = artistService.getArtistById(artistId);
        return new ResponseEntity<>(artist, HttpStatus.OK);
    }

    // Method to edit an artist
    @PutMapping("/artists/edit/{id}")
    public ResponseEntity<Artist> updateArtist(@PathVariable("id") Integer id, @RequestBody Artist artist) {
        artist.setArtistId(id);
        Artist updateArtist = artistService.updateArtist(artist);
        return new ResponseEntity<>(updateArtist, HttpStatus.OK);
    }

    // Method to delete an artist
    @DeleteMapping("/artists/delete/{id}")
    public ResponseEntity<String> deleteArtist(@PathVariable("id") Integer artistId) {
        artistRepository.deleteById(artistId);
        return new ResponseEntity<>("Artist successfully deleted!", HttpStatus.OK);
    }




}
