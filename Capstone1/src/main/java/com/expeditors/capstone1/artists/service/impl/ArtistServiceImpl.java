package com.expeditors.capstone1.artists.service.impl;

import com.expeditors.capstone1.tracks.entity.Track;
import lombok.AllArgsConstructor;
import java.util.List;
import org.springframework.stereotype.Service;
import java.util.Optional;
import com.expeditors.capstone1.artists.entity.Artist;
import com.expeditors.capstone1.artists.service.ArtistService;
import com.expeditors.capstone1.artists.repository.ArtistRepository;

@Service
@AllArgsConstructor
public class ArtistServiceImpl implements ArtistService {
    private ArtistRepository artistRepository;

    @Override
    public Artist createArtist(Artist artist) {
        return artistRepository.save(artist);
    }

    @Override
    public List<Artist> getAllArtists() {
        return artistRepository.findAll();
    }

    @Override
    public Artist getArtistById(Integer artistId) {
        Optional<Artist> optionalArtist = artistRepository.findById(artistId);
        return optionalArtist.get();
    }

    @Override
    public Artist updateArtist(Artist artist) {
        Artist existingArtist = artistRepository.findById(artist.getArtistId()).get();
        existingArtist.setFirstName(artist.getFirstName());
        existingArtist.setLastName(artist.getLastName());
        existingArtist.setCountry(artist.getCountry());
        Artist updateArtist = artistRepository.save(existingArtist);
        return updateArtist;
    }

    @Override
    public void deleteArtist(Integer artistID) {
        artistRepository.deleteById(artistID);
    }

}
